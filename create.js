const fs = require("fs");

const createPerson = function(person) {
    fs.writeFileSync("./person.json", JSON.stringify(person))
    return person;
}

const fadli = createPerson({
    "nama": "Fadli",
    "provinsi": "Jawa Tengah",
    "umur": 20,
    "status": "single"
})

module.exports = createPerson