const express = require('express');
const fadli = require("./person.json");

const launch = express();

launch.get('/', (req, res) => {
    res.render('index.ejs')
})

launch.get('/data', (req, res) => {
    res.render('data.ejs', {
        data: fadli
    })
})

launch.listen(3000);